module.exports = function(grunt){

  grunt.initConfig({
    project: {
      app: 'top_n_top_makeup'
    },
    sass: {
      dev: {
        options: {
          style: 'expanded'
        },
        files: {
          'media/styles/style.css': 'media/styles/application.scss'
        }
      },
      dist: {
        options: {
          style: 'compressed',
        },
        files: {
          'media/styles/style.min.css': 'media/styles/application.scss'
        }
      }
    },
    coffee: {
      dev: {
        options: {
          join: true,
          bare: true
        },
        files: {
          'media/scripts/scripts.js': 'media/scripts/*.coffee'
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'media/scripts/application.min.js': 'media/scripts/application.js'
        }
      }
    },
    concat: {
      dev: {
        src: ['media/scripts/vendor/*.js', 'media/scripts/scripts.js'],
        dest: 'media/scripts/application.js',
      },
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 version', '> 5%', 'ie >= 8']
      },
      dist: {
        src: ['media/styles/style.css', 'media/styles/style.min.css']
      }
    },
    sprites: {
      main: {
        src: ['media/images/sprite/*.png'],
        css: 'media/styles/_sprite.scss',
        map: 'media/images/sprile.png',
        dimensions: true,
        classPrefix: 'sprite'
      }
    },
    watch: {
      includes: {
        files: ['src/*.html', 'src/**/*.html'],
        tasks: 'includes:html'
      },
      sprites: {
        files: 'media/images/sprite/*.png',
        tasks: 'sprites:main'
      },
      /*uglify: {
        files: 'media/scripts/application.js',
        tasks: 'uglify:dist'
      },*/
      concat: {
        files: ['media/scripts/vendor/*.js', 'media/scripts/scripts.js'],
        tasks: 'concat:dev'
      },
      coffee: {
        files: 'media/scripts/*.coffee',
        tasks: 'coffee:dev'
      },
      sass: {
        files: 'media/styles/{,*/}*.{scss,sass}',
        tasks: ['sass:dev'/*, 'sass:dist'*/, 'autoprefixer:dist']
      },
      options: {
        livereload: true,
      }
    },
    connect: {
      server: {
        options: {
          port: 3000,
          base: '',
        }
      }
    },
    includes: {
      html: {
        src: 'src/*.html',
        dest: '',
        flatten: true,
        options: {
          silent: true,
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-imagine');
  grunt.loadNpmTasks('grunt-includes');

  grunt.registerTask('default', [
    'connect',
    'sass',
    'watch',
    'coffee',
    'concat',
    'uglify',
    'autoprefixer',
    'sprites',
    'includes',
  ]);

}